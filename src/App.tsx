import React from "react";
import "./App.scss";
import Gallery from "./features/gallery/Gallery";
import { Provider } from "react-redux";
import store from "./store";

const App = () => {
  return (
    <Provider store={store}>
      <div className="App">
        <header style={{ background: "#000" }}>
          <div
            style={{
              height: "50vh",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundImage:
                "url(https://images.pexels.com/photos/3704989/pexels-photo-3704989.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260)",
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
              backgroundAttachment: "fixed"
            }}
          >
            <div>
              <h1
                style={{
                  color: "white",
                  fontSize: "4rem",
                  textTransform: "uppercase"
                }}
              >
                Flickr Gallery
              </h1>
              <h4 style={{ color: "white" }}>
                The searchiest flickr gallery you will ever find!
              </h4>
              <p style={{ color: "white", background: "red" }}>
                NOTE!: there exist NSFW content...
              </p>
            </div>
          </div>
        </header>
        <section className="App-content">
          <Gallery />
        </section>
      </div>
    </Provider>
  );
};

export default App;
