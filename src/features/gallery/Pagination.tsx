import React from "react";
import { useDispatch } from "react-redux";

export const Pagination: React.SFC<any> = props => {
  const { currentPage, total, onClick, currentApiMethod, searchValue } = props;
  const dispatch = useDispatch();

  const handleOnClick = (page: number) => {
    dispatch(onClick(currentApiMethod, { text: searchValue, page: page }));
  };

  return (
    <>
      <button onClick={() => handleOnClick(1)}>First - 1</button>
      ..
      {currentPage > 3 && (
        <button onClick={() => handleOnClick(currentPage - 3)}>
          {currentPage - 3}
        </button>
      )}
      {currentPage > 2 && (
        <button onClick={() => handleOnClick(currentPage - 3)}>
          {currentPage - 2}
        </button>
      )}
      {currentPage > 1 && (
        <button onClick={() => handleOnClick(currentPage - 3)}>
          {currentPage - 1}
        </button>
      )}
      {/*  */}
      <button>Current page :{currentPage}</button>
      {/*  */}
      {currentPage + 1 < total && (
        <button onClick={() => handleOnClick(currentPage + 1)}>
          {currentPage + 1}
        </button>
      )}
      {currentPage + 2 < total && (
        <button onClick={() => handleOnClick(currentPage + 2)}>
          {currentPage + 2}
        </button>
      )}
      {currentPage + 3 < total && (
        <button onClick={() => handleOnClick(currentPage + 3)}>
          {currentPage + 3}
        </button>
      )}
      ..
      <button onClick={() => handleOnClick(total)}>{total} - Last</button>
    </>
  );
};

// Component so select how many images to fetch and show for each api call
export const SelectImageAmount: React.SFC<any> = props => {
  const { selectedAmount, onSelect } = props;
  const selectAmounts: Array<number> = [20, 50, 100, 200, 500];

  return (
    <select id="select-amount" value={selectedAmount} onChange={onSelect}>
      {selectAmounts.map(number => {
        <option value={number}>{number}</option>;
      })}
    </select>
  );
};
