import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// This is the initialized state and reducer for incoming state changes.

interface Pictures {
  page: number | null;
  pages: string | null;
  pepage: number | null;
  total: string | null;
  photo: Array<object>;
}
interface InitialState {
  pictures: Pictures;
  loading: boolean;
  error: string | null;
}

export const pics: Pictures = {
  page: null,
  pages: null,
  pepage: null,
  total: null,
  photo: [{}]
};

export const initialState: InitialState = {
  pictures: pics,
  loading: false,
  error: null
};

export const slice = createSlice({
  name: "gallery",
  initialState,
  reducers: {
    fetchPicturesStart: state => {
      state.loading = true;
    },
    fetchPicturesSuccess: (state, action: PayloadAction<Pictures>) => {
      state.pictures = action.payload;
      state.loading = false;
    },
    fetchPicturesError: (state, action: PayloadAction<string>) => {
      state.error = action.payload;
      state.loading = false;
    }
  }
});

export const {
  fetchPicturesStart,
  fetchPicturesSuccess,
  fetchPicturesError
} = slice.actions;

export default slice.reducer;
