/* eslint-disable @typescript-eslint/camelcase */
import { AppDispatch, AppThunk } from "../../store";
import {
  fetchPicturesStart,
  fetchPicturesSuccess,
  fetchPicturesError
} from "./GallerySlice";
import axios from "axios";

export enum ApiMethod {
  SEARCH = "search",
  GETRECENT = "getRecent"
}

const url = "https://www.flickr.com/services/rest/";

export const fetchGallery = (
  method: ApiMethod,
  params?: object
): AppThunk => async (dispatch: AppDispatch) => {
  dispatch(fetchPicturesStart());
  try {
    const response = await axios.get(url, {
      params: {
        api_key: process.env.REACT_APP_FLICKR_KEY,
        method: `flickr.photos.${method}`,
        format: "json",
        nojsoncallback: 1,
        ...params
      }
    });
    const value = response.data.photos;
    dispatch(fetchPicturesSuccess(value));
  } catch (e) {
    dispatch(fetchPicturesError("Something went wrong."));
  }
};

export const fetchGalleryRecent = (): AppThunk => async (
  dispatch: AppDispatch
) => {
  dispatch(fetchPicturesStart());
  try {
    const response = await axios.get(url, {
      params: {
        api_key: process.env.REACT_APP_FLICKR_KEY,
        method: "flickr.photos.getRecent",
        format: "json",
        nojsoncallback: 1
      }
    });
    const value = response.data.photos;
    dispatch(fetchPicturesSuccess(value));
  } catch (e) {
    dispatch(fetchPicturesError("Something went wrong."));
  }
};
export const fetchGalleryBySearch = (search: string): AppThunk => async (
  dispatch: AppDispatch
) => {
  dispatch(fetchPicturesStart());
  try {
    const response = await axios.get(url, {
      params: {
        api_key: process.env.REACT_APP_FLICKR_KEY,
        method: "flickr.photos.search",
        format: "json",
        nojsoncallback: 1,
        text: search
      }
    });
    const value = response.data.photos;
    dispatch(fetchPicturesSuccess(value));
  } catch (e) {
    dispatch(fetchPicturesError("Something went wrong."));
  }
};
