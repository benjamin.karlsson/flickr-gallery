import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchGalleryBySearch,
  fetchGallery,
  ApiMethod
} from "./GalleryActions";
import { Pagination } from "./Pagination";
import { RootState } from "store";
import "./Gallery.scss";

const Search: React.SFC<any> = props => {
  const [value, setValue] = useState("");
  const { loading, setApiRecent, setSearchValue } = props;
  const dispatch = useDispatch();
  const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
    setValue(event.currentTarget.value);
  };
  const onClick = (value: string) => {
    if (value.length > 0) {
      dispatch(fetchGalleryBySearch(value));
      setSearchValue(value);
      setApiRecent(ApiMethod.SEARCH);
    } else {
      dispatch(fetchGallery(ApiMethod.GETRECENT));
      setApiRecent(ApiMethod.GETRECENT);
    }
  };
  return (
    <>
      <input
        type="text"
        value={value}
        placeholder="search"
        onChange={handleChange}
      ></input>
      <button
        aria-label="search"
        onClick={() => onClick(value)}
        disabled={loading}
      >
        Search
      </button>
    </>
  );
};

const PhotoCard: React.FC<any> = props => {
  const { farm, id, server, secret, title } = props;
  const url = `https://farm${farm}.staticflickr.com/${server}/${id}_${secret}.jpg`;
  return (
    <article
      style={{
        position: "relative"
      }}
    >
      {/* Image Wrapper */}
      <div style={{ lineHeight: "0" }}>
        {/* Image */}
        <img
          src={url}
          alt={title}
          style={{
            backgroundColor: "white",
            width: "100%",
            height: "100%",
            lineHeight: "0",
            borderRadius: "6px"
          }}
        ></img>
      </div>
    </article>
  );
};

const Gallery: React.FC = () => {
  const pictures = useSelector((state: RootState) => state.gallery.pictures);
  const loading = useSelector((state: RootState) => state.gallery.loading);
  const error = useSelector((state: RootState) => state.gallery.error);

  const dispatch = useDispatch();

  const [currentApiMethod, setCurrentApiMethod] = useState(ApiMethod.GETRECENT);
  const [searchValue, setSearchValue] = useState("");

  useEffect(() => {
    dispatch(fetchGallery(ApiMethod.GETRECENT));
  }, []);

  return (
    <div className="gallery">
      {error && <p>{error}</p>}
      <Search
        loading={loading}
        setApiRecent={setCurrentApiMethod}
        setSearchValue={setSearchValue}
      />
      {loading && (
        <div>
          <span
            style={{
              color: "rgb(215,245,237)",
              textTransform: "uppercase",
              fontWeight: "bold"
            }}
          >
            ...Loading
          </span>
        </div>
      )}
      {error && <div>{error}</div>}
      {/* Gallery wrapper */}
      <br />
      {!loading && !error && (
        <>
          <Pagination
            currentPage={pictures.page}
            total={pictures.total}
            searchValue={searchValue}
            currentApiMethod={currentApiMethod}
            onClick={fetchGallery}
          ></Pagination>

          <div
            style={{
              display: "grid",
              gridTemplateColumns: "repeat(auto-fill, minmax(300px, 1fr))",
              gridGap: "1rem",
              padding: "1rem"
            }}
          >
            {pictures.photo.length > 1 &&
              pictures.photo.map((photo: any) => (
                <PhotoCard
                  key={photo.id}
                  farm={photo.farm}
                  id={photo.id}
                  server={photo.server}
                  secret={photo.secret}
                  title={photo.title}
                ></PhotoCard>
              ))}
          </div>
        </>
      )}
      {/*  */}
    </div>
  );
};

export default Gallery;
