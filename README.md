This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) using [cra-template-typekit](https://github.com/rrebase/cra-template-typekit)

# Flickr - Gallery

## Comments on Asignment

### Frameworks/Libraries used
Bootstraped the project with cra-template-typekit to get quickly up and running with react, typescript, axios and redux.
- React + Typescript
- Axios
- Redux Toolkit

#### Axios
A simple library to to http requests, I'm using it in `/features/gallery/GalleryActions.tsx` for making the api calls towards the Flicrk API.

#### Redux Toolkit
State machine for maniging states in one place instead of all over the application and each component. Makes it easier when the app grows and structuring the code.
- `GallerySlice.tsx` is where 

#### Typescript
I've mostly written vanilla javascript so the typescript might be a little bit wonky. Added typescript in this project for the assignment sake, but I dont mind working with it and learning more. This is probably where is spent most my time during the assignment.

### Changes and Updates
- Adding javascript for making the grid like a masonry
- add styling to buttons and search field.
- move search field in to the jumbotron/header.
- add a dropdown for how many images to show on each pagination
- add some kind of filter for NSFW.
- a menu for showing - most recent - most popular - latest updated - etc.
- a menu for searching by tags.


## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

### `test:coverage`

Run all tests with `--coverage`. Html output will be in `coverage/`.

### `yarn lint`

Lints the code for eslint errors and compiles TypeScript for typing errors.
Update `rules` in `.eslintrc` if you are not satisifed with some of the default errors/warnings.

### `yarn lint:fix`

Same as `yarn lint`, but also automatically fixes problems.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
